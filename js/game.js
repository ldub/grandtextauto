//Define global game logic variable
var GTA = {};
//$(GTA.GameOutputSelector) should select the element to output game info to.
GTA.GameOutputSelector = ".game-output";
//User's inventory
GTA.Inventory = [];

//Helper functions
GTA.Helpers = {
	//moves the user to a room only if it is adjacent to his current room
	MoveToRoom: function(roomId) {
		if (typeof(GTA.CurrentRoomId) === 'undefined') {
			//This only happens once, at the very beggining of the game
			GTA.CurrentRoomId = "bedroom";
			GTA.Helpers.OutputNoInput(GTA.Helpers.GenerateRoomInfo(GTA.CurrentRoomId));
		} else {
			var oldRoomId = GTA.CurrentRoomId;
			//Check if the room to move to is adjacent to the current room.
			if (GTA.GameMap.Rooms[GTA.CurrentRoomId].AdjacentRoomIds.indexOf(roomId) != -1) {
				GTA.CurrentRoomId = roomId;
				GTA.Helpers.Output("goto " + roomId, GTA.Helpers.GenerateRoomInfo(roomId));
			} else {
				GTA.Helpers.Output("goto " + roomId, "No such room.");
			}
		}
	},
	//Generates the info that is given to the user upon stepping inside a room.
	GenerateRoomInfo: function(roomId) {
		var currentRoom = GTA.GameMap.Rooms[roomId];
		var outputString = currentRoom.Description + "<br /> Items: ";
		for (var i = 0; i < currentRoom.Items.length; i++) {
			//output items as: Wallet (Take)
			outputString += currentRoom.Items[i].Name + " (" + currentRoom.Items[i].Action + ")";
			if (i != currentRoom.Items.length - 1) {
				outputString += ", ";
			}
		}
		//Adjacent rooms
		outputString += "<br /> You can goto: " + currentRoom.AdjacentRoomIds;
		return outputString;
	},
	//The next three functions (TakeItem, OpenItem, StealItem) are very similar.
	//It was actually a mistake to separate them into three functions.
	TakeItem: function(itemName, numberOfUses) {
		var currentRoom = GTA.GameMap.Rooms[GTA.CurrentRoomId];
		//iterate through all the items in the room to check if the item that goes by itemName is in the current room
		for (var i = 0; i < currentRoom.Items.length; i++) {
			//check if the item is in the room and is supposed to be "taken"
			if (currentRoom.Items[i].Name.toLowerCase() == itemName && currentRoom.Items[i].Action.toLowerCase() == "take") {
				//Check if the user has the required items in his inventory
				var eligible = GTA.Helpers.ConfirmItemEligibility(i);
				if (eligible) {
					//Add item to inventory
					GTA.Inventory.push(new GTA.InventoryItem(itemName, numberOfUses));
					//Tell the user that this was a success
					GTA.Helpers.Output("take " + itemName, "You took " + itemName + ".");
					//Update the inventory display
					GTAUI.Display.UpdateInventory();
				}
				return;
			}
		}
		GTA.Helpers.Output("take " + itemName, "No such item.");
	},
	OpenItem: function(itemName, numberOfUses) {
		var currentRoom = GTA.GameMap.Rooms[GTA.CurrentRoomId];
		for (var i = 0; i < currentRoom.Items.length; i++) {
			if (currentRoom.Items[i].Name.toLowerCase() == itemName && currentRoom.Items[i].Action.toLowerCase() == "open") {
				var eligible = GTA.Helpers.ConfirmItemEligibility(i);
				if (eligible) {
					GTA.Inventory.push(new GTA.InventoryItem(currentRoom.Items[i].Reward, numberOfUses));
					GTA.Helpers.Output("open " + itemName, "After opening " + currentRoom.Items[i].Name + ", you recieved " + currentRoom.Items[i].Reward + ".");
					GTAUI.Display.UpdateInventory();
				}
				return;
			}
		}
		GTA.Helpers.Output("open " + itemName, "No such item.");
	},
	StealItem: function(itemName, numberOfUses) {
		var currentRoom = GTA.GameMap.Rooms[GTA.CurrentRoomId];
		for (var i = 0; i < currentRoom.Items.length; i++) {
			if (currentRoom.Items[i].Name.toLowerCase() == itemName && currentRoom.Items[i].Action.toLowerCase() == "steal") {
				var eligible = GTA.Helpers.ConfirmItemEligibility(i);
				if (eligible) {
					GTA.Inventory.push(new GTA.InventoryItem(currentRoom.Items[i].Reward, numberOfUses));
					GTA.Helpers.Output("steal " + itemName, "You're slick! Got away with " + currentRoom.Items[i].Reward + ", no problemo.");
					GTAUI.Display.UpdateInventory();
				}
				return;
			}
		}
		GTA.Helpers.Output("steal " + itemName, "No such item.");
	},
	//Checks if the user has all required items needed to pick up item in question
	ConfirmItemEligibility: function(itemIndex) {
		//item in question
		var item = GTA.GameMap.Rooms[GTA.CurrentRoomId].Items[itemIndex];
		//does this item require other items?
		if (item.RequiredItems != null) {
			//assume the user does have all other required items
			var eligibleTotal = true;
			//iterate through all required items
			for (var j = 0; j < item.RequiredItems.length; j++) {
				//assume user does not have the current require item in question
				var eligibleThisItem = false;
				//iterate through the user's inventory
				for (var k = 0; k < GTA.Inventory.length; k ++) {
					//if the user has the current required item in question, mark eligibleThisItem as true
					if (item.RequiredItems[j] == GTA.Inventory[k].Name) {
						eligibleThisItem = true;
					}
				}
				//if the user does not have any required item, mark eligibleTotal as false 
				if (!eligibleThisItem) {
					eligibleTotal = false;
					//tell the user what items he is missing
					GTA.Helpers.Output("steal " + item.Name, "You're missing " + item.RequiredItems[j] + ". Its required.");
				}
			}
			//this will return whether the user is eligible for the item in question.
			return eligibleTotal;
		} else {
			//no items required, so user is automatically eligible.
			return true;
		}
	},
	//Output mimicks a WriteLine() function. In other words, a <br /> is always appended.
	Output: function(input, output) {
		$(GTA.GameOutputSelector).append("<span class='command'>> "+ input + "</span><br/>" + output + "<br />");
		this.ScrollToBottom();
	},
	OutputNoInput: function(output) {
		$(GTA.GameOutputSelector).append(output + "<br />");	
		this.ScrollToBottom();
	},
	//Scrolls to the bottom of the game-output div with a fancy animation
	ScrollToBottom: function() {
		$(GTA.GameOutputSelector).animate({scrollTop: $(GTA.GameOutputSelector).prop("scrollHeight")}, 500);
	}
};

GTA.Map = (function(){
	function Map() {

	}

	Map.prototype.AddRoom = function(room) {
		//obsolete typecheck
		if (room.Type === "GTA.Room") {
			//add room
			this.Rooms[room.Id] = room;
		}
	};

	//I was doing some fake typechecking for a while. I decided that TypeScript is what I should use if I want to actually
	//  have typechecking, all other efforts are pretty futile.
	Map.prototype.Type = "GTA.Map";

	Map.prototype.Rooms = {}

	return Map;
})();

//the following objects (Room, Item, InventoryItem) are fairly self-explanatory.
GTA.Room = (function(){
	'use strict';

	// Private variables

	// Private function
	var bar = function () { return 25; };

	// Constructor
	function Room(Name, ID, Description, AdjacentRoomIds, Items) {
	  this.Name = Name;
	  this.Id = ID;
	  this.Description = Description;
	  this.AdjacentRoomIds = AdjacentRoomIds;
	  this.Items = Items;
	}    

	Room.prototype.Type = "GTA.Room";

	return Room;
})();

GTA.Item = (function(){
	function Item(Name, Action, Reward, RequiredItems, RemoveOriginal) {
		this.Name = Name;
		this.Action = Action;
		this.Reward = Reward;
		this.RequiredItems = RequiredItems;
		this.RemoveOriginal = RemoveOriginal;
	}

	Item.prototype.Type = "GTA.Item";

	return Item;
})();
//the difference between Item and InventoryItem is that InventoryItem is actually inside the inventory and therefore
//  does not need several properties such as RequiredItems
GTA.InventoryItem = (function(){
	function InventoryItem(Name, NumberOfUses) {
		this.Name = Name;
		//NumberOfUses exists, but doesnt really do anything.
		this.NumberOfUses = NumberOfUses;
	}

	return InventoryItem;
})();

GTA.InputParser = (function(){
	function InputParser() {

	}

	InputParser.prototype.Parse = function(input) {
		//this code is a mistake
		//a better code would be to have a GTA.Helpers.PerformAction(ActionName, ObjectName) function that
		//  contains a switch statement. It would remove the need for three (four) separate functions
		//  for actions.

		//lowercase the input for convenience
		input = input.toLowerCase();
		//the parses assumes that all commands are followed by a space ("pick up" cannot be a command, the 
		//  parse would assume that "pick" is the command)
		var command = input.split(" ")[0];
		//This code should be fairly self-explanatory
		switch (command) {
			case "goto":
				var roomId = input.substring(5);
				GTA.Helpers.MoveToRoom(roomId);
			break;
			case "take":
				var itemName = input.substring(5);
				GTA.Helpers.TakeItem(itemName, 1);
			break;
			case "open":
				var openName = input.substring(5);
				GTA.Helpers.OpenItem(openName, 1);
			break;
			case "steal": 
				var stealName = input.substring(6);
				GTA.Helpers.StealItem(stealName, 1);
			break;			
			default:
				GTA.Helpers.Output(input, "Come again?");
			break;
		}
	};

	return InputParser;
})();