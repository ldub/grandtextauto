$(function(){
	//InputParser is what... parses the user's input.
	var inputParser = new GTA.InputParser();

	//Handle text input from #text-input
	$("#text-input").keypress(function(event){
		if (event.which == 13) {
			inputParser.Parse($("#text-input").val());
			//Clear the text-input space
			$("#text-input").val("");
		}
	});

	//Defines the map and its items:
	GTA.GameMap = new GTA.Map();
	//For reference while adding rooms: Room(string Name, string Id, string Description, array AdjacentRoomIds, array Items)
	//For reference while adding items: Item(string Name, string Action, string Reward, array RequiredItems, bool RemoveOriginal)
	GTA.GameMap.AddRoom(
		new GTA.Room(
			"Bedroom", //string Name
			"bedroom",         //string Id
			"You're in your bedroom, at your house in Lincoln Park.", //string Description
			["bathroom","living room"], //array AdjacentRoomIds
			[ //array Items
				new GTA.Item("Wallet", "take", "Wallet", [], true)
			]
		)
	);
	GTA.GameMap.AddRoom(
		new GTA.Room(
			"Kitchen", //string Name
			"kitchen",         //string Id
			"You step into the kitchen to the everlasting smell of burnt plastic.", //string Description
			["living room"], //array AdjacentRoomIds
			[] //dictionary Items
		)
	);
	GTA.GameMap.AddRoom(
		new GTA.Room(
			"Bathroom", //string Name
			"bathroom",         //string Id
			"After walking into the bathroom, you resolve to do your business while holding your nose and get out as quick as possible.", //string Description
			["bedroom"], //array AdjacentRoomIds
			[] //dictionary Items
		)
	);
	GTA.GameMap.AddRoom(
		new GTA.Room(
			"Saferoom", //string Name
			"saferoom",         //string Id
			"Your state-of-the art saferoom is perhaps the only thing you're proud of in this whole house. With fingerprint and vocal security, you're sure that nobody else can step inside.", //string Description
			["living room"], //array AdjacentRoomIds
			[
				new GTA.Item("Cabinet", "open", "$50", ["wallet"], true),
				new GTA.Item("Gun Safe", "open", "Glock 19", [], false)
			] //dictionary Items
		)
	);
	GTA.GameMap.AddRoom(
		new GTA.Room(
			"Living Room", //string Name
			"living room",         //string Id
			"You've entered the living room and are now deciding whether or not to take a long-needed nap on the couch.", //string Description
			["bedroom", "kitchen", "saferoom","sheffield ave"], //array AdjacentRoomIds
			[
				new GTA.Item("TV Remote", "take", "TV Remote", [], true)
			] //dictionary Items
		)
	);
	GTA.GameMap.AddRoom(
		new GTA.Room(
			"Sheffield Ave", //string Name
			"sheffield ave",         //string Id
			"Its raining, as usual. Good thing there's a car nearby.", //string Description
			["living room"], //array AdjacentRoomIds
			[
				new GTA.Item("Car", "steal", "Nissan Leaf", ["Glock 19"], true)
			] //dictionary Items
		)
	);


	//Start the game
	GTA.Helpers.MoveToRoom("bedroom");
});