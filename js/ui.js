//Defining global UI variable
GTAUI = {};
//State holds the state of various things
GTAUI.State = {
	"displayOpen" : true 
};
GTAUI.Display = {};
GTAUI.Display.Tabs = {};

//Toggles the display on the right with a fancy animation.
GTAUI.Display.ToggleDisplay = function() {
	GTAUI.Display.ReverseSlider();
	if (GTAUI.State["displayOpen"]) {
		GTAUI.Display.SlideDisplayIn();
	} else {
		GTAUI.Display.SlideDisplayOut();
	}
	GTAUI.State["displayOpen"] = !GTAUI.State["displayOpen"];
}
//Animation
GTAUI.Display.SlideDisplayIn = function() {
	$(".game-info").animate({
		"right" : "-14px"
	}, 1500);
}
GTAUI.Display.SlideDisplayOut = function() {
	$(".game-info").animate({
		"right" : "-215px"
	}, 1500);
}
//Reverses the slider button to match whether the display is open or not
GTAUI.Display.ReverseSlider = function() {
	if (GTAUI.State["displayOpen"]) {
		$(".slider-button-text").html("> <br /> > <br /> > <br /> ");
	} else {
		$(".slider-button-text").html("< <br /> < <br /> < <br /> ");
	}
}
//Controls the "Status, Inventory, Help" tabs on top of the display.
GTAUI.Display.Tabs.SwitchTab = function(e) {
	$(".tab-button.active").toggleClass("active");
	$(e.target).toggleClass("active");
	var tabId = e.target.attributes["href"].nodeValue;
	$(".tab.active").toggleClass("active");
	$(tabId).toggleClass("active");
}
//This function is used when the user picks up an item.
//It updates the inventory in the display.
GTAUI.Display.UpdateInventory = function() {
	var inventoryString = "";
	for (var i = 0; i < GTA.Inventory.length; i ++) {
		inventoryString += "<div class='inventory-item'>" + GTA.Inventory[i].Name + "</div>";
	}
	$(".tab.inventory").html(inventoryString);
}
$(function(){	
	//Register handler for the display slider button
	$(".slider-button").click(GTAUI.Display.ToggleDisplay);
	//Register handler for the display tabs.
	$(".tab-button").click(
		GTAUI.Display.Tabs.SwitchTab
	);
	//start a new game by refreshing the game
	$(".button.newgame").click(function(){
		window.location.reload();
	});
});